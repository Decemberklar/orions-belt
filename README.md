
Application Name	Orion's Belt (Abbv, OB+)     
Application Version 	v1.3.00 (Betelgeuse Branch)     
Developer		Kyle J. "Decemberklar" Gagliardo     

.....................................................................................................

Quick Install:

Extract ZIP file to location of choice, enter directory, right click Orion's Belt(.exe) and 
Send To Desktop. Utilize the shortcut to avoid any possible errors by scrambling the core directory layout. 

.....................................................................................................

Purpose:

To potentially eliminate possible errors during the Defective Part Tag Processing due to the 
repetitive nature. Orion's Belt is a "middle man" service, that is worked in instead of Orion, 
allowing the end-user to fully set a pallet via only DPT numbers (removing the requirement of
the serial number, as it is obtained during the automation process).

.....................................................................................................

Known Issues:
- The Right Hand list is not utilized currently during the processing process.
  Update 03/06: Right List will be truncuated for the time being with the upcoming inclusion of
	"Half Mode", which will focus on the left list only. Right list will be a focus the week
	we see X-series in the wipe queue lane.
  Update 04/26: With v1.3, currently implementing the logic across the app for the right hand list.

.....................................................................................................

Changelog: 

	.....v1.2.16 -> v1.3.00......................................................................

	- Redesigned and configured processing method. 
		More concisely written, and better logic. Also, works with second list, see next bullet.

	- Second list implementations. Currently being weeded in, but not officially ready yet.

	- Tooltip will pop-up during processing showing the current processing number and out of X.

	- DevMode Hotkey. Currently prevents items from being sent off during processing.
		Note: Removed Edit & Variable List debugging options from compiled version.

	- ErrorWatching implemented for SB/XA Orion, but needs further testing (not enabled).

	- Can no longer save empty log files. Good for when you accidently hit CTRL+S

	- Added second save option that allows you to append a note to the front of the log title.
		Good for distinguishing if it's a partial or mixed pallet, etc.

	- Removed "Un-Saved Content Blah Blah" Message Box when purging after a save. A normal purge
		will still ask if you'd like to delete the list.

	- Reorganized Saved Log names
		CustomNote MonthAbbr Day AssetName Time.txt
		Eventually want to make this customizable anyways.
	
	- Added ALT activation to DPT button. Why this has been missing this entire time is beyond me.

	.....v1.2.14 -> v1.2.16......................................................................

	- OB+ will now load with the Part Number field focused immediately, after inputting a PN, the 
		control will shift to the DPT entry field. This should make using the app more natural.

	....v1.2.13a -> v1.2.14......................................................................

	- New Option: Legacy Mode.
		This is for swapping between Old/New Orion. Checking the box implies Old, and vise-versa.

	....v1.2.13a -> v1.2.14......................................................................

	- Changed 450 Handling. 
		Now OB will check for a 450 code, if it's missing, will add it prior to the 200/210. 
		At the end of processing, it will still display to you any possible missing codes 
		so that you may personally log the asset. 

		I believe this is the best to avoid a possible wildfire if in fact those assets were
		not properly combed.


	....v1.2.12a -> v1.2.13a.....................................................................

	- New log naming convention.
		Date.PartDescription.Time.txt
		Example: 04.14_Dell.Mini_02.34.txt

	- Script will now wait ~1s for required windows to appear and be active prior to continuing.
		Required Windows: Repair DPT Update, SB+ Dialog, SB+ Prompt

	- Created a Lang file to call on for information strings for easier updating.
	
	....v1.2.10a -> v1.2.12a.....................................................................

	- Implemented 450 code checking
	  Currently this will just display the codes missing in a window at the end of the processing.

	- Started more checks for specific windows required during the processing phase to help ensure accuracy.

	- CTRL+S & CTRL+L hotkeys added for quick save/load (respective). Only works when OB+ is active and 
	  forefront.
	
	....v1.2.05a -> v1.2.10a.....................................................................

	- Various Fail-Safe Tweaks for maximum accuracy. 	
		+ DPT Processing will not do entries with a Serial Number
		+ Pallet Processing will not do entries with a "X" in the Processed col.
		+ This is so when an error is detected, the loop can start from the beginning to ensure the next 
		  code/command is successful.

	....v1.2.02a -> v1.2.05a.....................................................................

	- Error Handling Beta Test. If SB+ Dialog/Error/Prompt messages show up, OB+ will now attempt
	  to decipher the severity of the issue and ask for your intervention on specific problems.

	- Reorganized Menu Bar
		+ Added option to open log directory
		+ Added option to open current loaded log into Notepad.

	- Added third column for fully processed DPT codes.
		A DPT processed code should generally host a Serial Number.
		This third col will show that the code has been both, DPT and Pallet processed.

	- Adjusted DPT entry wait timer to compensate for possible edited Scanner ms
	  This should rectify possible issues with Duplicate Codes not being purged and looping errors.

	- Added option for pallet processing speed (default 0.5s [500])

	- Implemented a counter to show the current amount of entries in the list

	- Loading a log will now purge the current list of data (unsaved progress will be lost).

	- Increased option edit field width (used to be dependent on original data size)

	- Whenever a DPT number has been either added or edited, it will be shifted into the visible plane.

	- Fixed hanged "Log File Successfully Loaded" message.

	- Better display of Application messages. Specifically DPT & Pallet success messages

	- Small delay between loading logs to prevent threads jumping too fast and not populating the list at all.

	- Various rewording and UI tweaks.

	- Reduced DPT column size by 17px to avoid horizontal scrolling when list begins to fill

	- Changed Pause key to something logical, like say, Pause.	

	- Added some additional app->you communication, through the status bar, brief messages, and even sound beeps!

	....v1.2.01a -> v1.2.02a.....................................................................


	- Added Hand Scanner Latency option.

	....v0.0.05a -> v1.2.01a.....................................................................

	- New Build Branch Title (Bellatrix) marks new functionality, reliability, and application
	  milestones!

	- Complicated Hotkeys are a thing of the past! Now Orion's Belt is packaged with a fully 
	  functional GUI to essentially replace the need to deal with Orion's clunky interface!

	- Full Pallet DPT Processing. Simply add all your DPT numbers for a given pallet into the
	  interface, start Orion->DPT Entry, and click the DPT button and away it goes! During
	  this process, each DPT code will automatically snag it's associated serial number and
	  insert it into the OB+ application for...

	- Full Pallet Building. Once you've processed all your DPT entries and populated the SN
	  field. Once you've manually constructed a pallet via Pallet Master, you can press the 
	  "Pallet" button to automatically insert all the numbers into a given pallet!

	- Extensible Options. Things change, it's best to be able to change along with it.
	  Current options include, Mode200 LOG Code (Default LOG04), and DPT Wait Time (Def 1000).

