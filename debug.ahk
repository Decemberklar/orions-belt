;#Warn, All
^!NumpadRight::Reload				; Reload Hotkey
^!NumpadLeft::Edit					; Edit Hotkey
^!NumpadDown::ListVars 				; ListVars Hotkey
^!NumpadUp::
	IMPULSE101 := ! IMPULSE101
	ToolTip % "Developer Mode: " (IMPULSE101 ? "Enabled":"Disabled")
	Sleep, 500
	ToolTip,
Return
Pause::Pause, Toggle  				; Pause App


