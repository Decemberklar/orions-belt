ClearStatusBar:
	; Apparently this is needed to prevent the StatusBar from saying DPT Mode Changed...
	SB_SetText("")
Return

DPTModeListen:
	
	; Change the application DPT mode based on the corresponding radio buttons.

	Gui, Submit, NoHide

	Cur_DPTMode := ( Process1Sel ? 200 : 210 )
	
	SB_SetText(LNG.STATUS_DPT_CHANGED Cur_DPTMode)

Return

; All of the following 2 section GUI elements should be able to be truncuated
; into a single routine...

PNEnterLis1:
	PartNumEntry(1)
Return

PNEnterLis2:
	PartNumEntry(2)
Return

DPTEnter1:

	DPTEntry(1)

	SendInput, ^a{Backspace}

Return

DPTEnter2:

	DPTEntry(2)

	SendInput, ^a{Backspace}

Return

PNUnlockLis1:
	ReleaseList(1)
Return

PNUnlockLis2:
	ReleaseList(2)
Return

ListLis1:

	If A_GuiEvent = "ColClick"
		Cur_SelList := 1	

Return

ListLis2:

	If A_GuiEvent = "ColClick"
		Cur_SelList := 2

Return

ContextEdit:
Return

ContextDelete:

	RowNumber := LV_GetNext(%A_EventInfo%, "F")
	LV_Delete(RowNumber)

	UpdateCounters()

Return

MenuHandler:
Return

MenuOptions:
	BuildOptions()
Return

OptionsGuiClose:
	Gui, Options: Destroy
Return

OptionsButtonSubmit:
	OptionsChanged()
Return

MenuFileOpen:

	FileSelectFile, SourceFile, 3, %A_ScriptDir%\logs, Select a Log File to Load., Text Documents (*.txt)
	If SourceFile =
    	Return  ; No File, No reason to be here anymore.

    Go := LoadLog(SourceFile)
    If (!Go) {
    	MsgBox % "Unable to load " SourceFile ". Please Try Again."
    }

    UpdateCounters()

    SB_SetText("Log File " LoadedLogName " Successfully Loaded")

Return

MenuFileSave:

	Go := SaveLog()
	If (!Go) {
		; Error Processing
	}
	Else {
		MsgBox, 4, OB+: Purge List?, % LNG.STATUS_SAVE_PURGE
		IfMsgBox Yes
		{
			Go := ListPurge(True)
		}
	}

Return

MenuFileSaveNote:

	Go := SaveLog(True)
	If (!Go) {

	}
	Else {
		MsgBox, 4, OB+: Purge List?, % LNG.STATUS_SAVE_PURGE
		IfMsgBox Yes
			Go := ListPurge(True)
	}

Return

GuiContextMenu:

	If (A_GuiEvent = "RightClick"){

		MouseGetPos, mX, mY

		If (A_GuiControl = "ListSel1")
		{

			Gui, ListView, ListSel1
				Menu, ListContext, Show, %mX%, %mY%

		} Else If (A_GuiControl = "ListSel2") {

			Gui, ListView, ListSel2
				Menu, ListContext, Show, %mX%, %mY%

		} Else {

			Return

		}

	}

Return

ListEdit:

	; Opens currently loaded log in notepad for manual editing.

	If (Cur_LoadedLog != "") {
		Run, Notepad %Cur_LoadedLog%
	}

Return

ListFolder:

	; Opens Log Storage Folder (assumed A_ScriptDir/Logs)

	Run, Explore %A_ScriptDir%\logs

Return

ListPurge:

	; Purges List w/ Confirmation

	ListPurge()

Return

EscapeApp:
GuiClose:
ExitApp 


#IfWinActive, ahk_exe Orion's Belt.exe

	; Use CTRL+S & CTRL+L for OB+ if active

	^s::GoTo, MenuFileSave
	^l::Goto, MenuFileOpen

#IfWinActive
	; Return back to other applications