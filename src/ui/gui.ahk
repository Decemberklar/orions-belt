#Include %A_ScriptDir%/src/ui/gui_controls.ahk
#Include %A_ScriptDir%/src/ui/gui_functions.ahk

; Build the Application GUI.
BuildGUI() {
	Global

;; Application Menu Bar
	Menu, FileMenu, Add, &Load Log, MenuFileOpen
	Menu, FileMenu, Add, &Save Log, MenuFileSave
	Menu, FileMenu, Add, Save Log (w &Note), MenuFileSaveNote
	Menu, FileMenu, Add
	Menu, FileMenu, Add, &Options, MenuOptions
	Menu, FileMenu, Add
	Menu, FileMenu, Add, E&xit, EscapeApp
	Menu, ListMenu, Add, &Manual Log Edit, ListEdit
	Menu, ListMenu, Add, Open Log Folder, ListFolder
	Menu, ListMenu, Add
	Menu, ListMenu, Add, P&urge Current Log, ListPurge
	Menu, GuiMenuBar, Add, &File, :FileMenu  
	Menu, GuiMenuBar, Add, &Log Management, :ListMenu
	Gui, Menu, GuiMenuBar

;; LEFT Side Controls ; Unlike the right side, this is at the top to maintain immediate focus.
	; PN Entry Field
	Gui, Add, Text, y58 x10,Part
	Gui, Add, Edit, vPNEnterSel1 gPNEnterLis1 x37 y55 w145 h20,
	; PN Entry Unlock Button
	Gui, Add, Button, vPNUnlockSel1 gPNUnlockLis1  x183 y55 w20 h20 Disabled, U
	; DPT Entry Field
	Gui, Add, Text, y83 x10,DPT
	Gui, Add, Edit, vDPTEnter1 gDPTEnter1 x37 y80 w165 h20 +WantReturn,

;; Interaction Controls
	; Process Buttons
	Gui, Add, Button, vProcessDPTSel gProcessDPTListen x312 y13 w50 h30, &DPT	   ; DPT
	Gui, Add, Button, vProcessPalSel gProcessPalListen x362 y13 w50 h30, &Pallet   ; Pallet

	; 200/210 Radio Buttons
	Gui, Add, Radio, vProcess1Sel gDPTModeListen x122 y20 w85 h20 +Checked, Resale (200)
	Gui, Font, s06, Verdana 
	;Gui, Add, Text, vLogCodeDisplay x150 y35, %Opt_LogCode%
	Gui, Font
	Gui, Add, Radio, vProcess2Sel gDPTModeListen x210 y20 w85 h20, Reuse (210)




	; Entry ListView
	Gui, Add, ListView, vListSel1 gListLis1  x12 y109 w190 h480 AltSubMit -ReadOnly +Report +Grid LVS_SHOWSELALWAYS, DPT Code|Serial No.|P
	LV_ModifyCol(1,76)		; DPT Number
	LV_ModifyCol(2,93)		; Serial Number
	LV_ModifyCol(3, "Auto") ; Processed Indicator

	;Count Label
	Gui, Add, Text, vTextSel1 x185 y593 w25 h12, 
	Gui, Add, Text, vTextSel2 x220 y593 w25 h12, 

;; RIGHT Side Controls
	; PN Entry Field
	Gui, Add, Edit, vPNEnterSel2 gPNEnterLis2  x242 y55 w145 h20 Disabled, 
	; PN Entry Unlock Button
	Gui, Add, Button,  vPNUnlockSel2 gPNUnlockLis2 x220 y55 w20 h20, U

	; DPT Entry Field
	Gui, Add, Edit, vDPTEnter2 gDPTEnter2 x222 y80 w165 h20 +WantReturn,

	; Entry ListView
	Gui, Add, ListView, vListSel2 gListLis2  x222 y109 w190 h480 AltSubmit Disabled +Report +Grid LVS_SHOWSELALWAYS, DPT Code|Serial No.|P
	LV_ModifyCol(1,76)		; DPT Number
	LV_ModifyCol(2,93)		; Serial Number
	LV_ModifyCol(3, "Auto") ; Processed Indicator

;; Misc GUI Elements
	; Status Bar
	Gui, Add, StatusBar,,

	; List Context Menu
	;Menu, ListContext, Add, Modify Entry, ContextEdit
	Menu, ListContext, Add, Delete Entry, ContextDelete

;; Init GUI
	Gui, Show, x1365 y126 h633 w426, %appName% (v%appRelease%)

	Return
}

; Create the Options Menu GUI
BuildOptions(){
	Global

	Gui, Options:+owner1		; Attach to Base GUI

	Gui, Options:Add, Text,, Current Log Code
	Gui, Options:Add, Edit, vOptLogCode w100, %Opt_LogCode%
	Gui, Options:Add, Text,, DPT Processing Wait Time
	Gui, Options:Add, Edit, vOptDptWait w100, %Opt_WaitTime%
	Gui, Options:Add, Text,, Pallet Processing Speed
	Gui, Options:Add, Edit, vOptPalWait w100, %Opt_PalWait%
	Gui, Options:Add, Text,, Hand Scanner Latency
	Gui, Options:Add, Edit, vOptScanWait w100, %Opt_ScanWait%

	;Gui, Options:Add, Text,, Default Save Mode
	;Gui, Options:Add, Radio, vSaveMode1 Checked, Basic Save Mode
	;Gui, Options:Add, Radio, vSaveMode2, Save With Note

	Gui, Options:Add, Text,, 
	Gui, Options:Add, Checkbox, vOptLegacyOrion Checked%Opt_LegacyMode%, Legacy Orion Mode
	Gui, Options:Add, Text,,
	
	Gui, Options:Add, Button, Default, Submit

	Gui, Options:Show,, Options: %appName% (v%appRelease%)

	Return
}