PartNumEntry(list) {
	Global

	; Listen for entered code, process and verify it's a Part Number then lock it in.

	Sleep, %Opt_ScanWaitInc%			; Default ~150
	GuiControlGet, PNEnterSel%list%		

	PartNum := PNEnterSel%list%

	If (StrLen(PartNum) == 11) {	; Part Numbers are generally 11 chars long

		If (PartNum != "") {

			Cur_SelList := list
			GuiControl, Disabled, PNEnterSel%list%
			SB_SetText("Part Number: [ " PartNum " ] Has Been Set.")
			UnlockList(list)

			Return True
		}

	} Else {

		SB_SetText("Part " LNG.ERR_INVALID)
		GuiControl,, PNEnterSel%list%

		Return False
	}

}

DPTEntry(list) {
	Global

	; Listen for DPT code, verify it's a DPT code, then append it to the list view.

	Sleep, %Opt_ScanWaitInc%

	GuiControlGet, DPTEnter%list%

	DPT := DPTEnter%list%

	If (StrLen(DPT) == 8) {		; DPT Codes are 8 characters long

		If (SubStr(DPT, 1, 1) == "D") {		   ; and start with D

			Gui, ListView, ListSel%list%

			Loop % LV_GetCount()
			{
				LV_GetText(DUPECHK, A_Index)
				If (DUPECHK = DPT) {		  ; Duplicate, Warn and Skip.

					SoundBeep, 2000
					SB_SetText("Previous Code [ " DPT " ] Not Entered. Reason: Duplicate")
					Return
				}
			}

			GoInt := LV_Add("", DPTEnter%list%, "")
			LV_Modify(GoInt, "Vis")		; Keep visibility on entered codes.
			
			UpdateCounters()

			Sleep, 100
		}
	} 

	Return
}

ReleaseList(list) {

	; Re-Enable the Part Number field for changes.

	GuiControlGet, PNEnterSel%list%
	GuiControl, Enabled, PNEnterSel%list%
	SendInput, {Shift}+{Tab}
	SendInput, ^a

	Return
}

UnlockList(list) {

	; Unlock associated list views (mostly for List #2)

	GuiControl, Enabled Show, PNUnlockSel%list%
	GuiControl, Enabled Show, ListSel%list%

	Return
}

OptionsChanged(){
	Global

	; Write to the application cfg file changes made.
	; Any changes that are not different from default will not
	; be written to the config file.

	Gui, Options:Submit, NoHide

	; Resale Log Code
	UpdateIni("200LogCode", "Options", OptLogCode, "LOG04")
		Opt_LogCode := OptLogCode

	; Time between each action during DPT Processing
	UpdateIni("DPTWaitTimes", "Options", OptDptWait, "1000")
		Opt_WaitTime := OptDptWait

	; Time between each entry during Build Pallet
	UpdateIni("PALWaitTimes", "Options", OptPalWait, "500")
		Opt_PalWait := OptPalWait

	; Time before OB+ cuts off input for a DPT number
	UpdateIni("ScannerListen", "Options", OptScanWait, "100")
		Opt_ScanWait := OptScanWait

	; Switch between Old/New Orion
	UpdateIni("LegacyMode", "Options", OptLegacyOrion, 1)
		Opt_LegacyMode := OptLegacyOrion

	Gui, Options:Destroy

	Return
}

UpdateCounters(){

	; Display the current count of assets in each list.

	Gui, ListView, ListSel1
	ListOne := LV_GetCount()
	
		GuiControl,, TextSel1, %ListOne% 

	Gui, ListView, ListSel2
	ListTwo := LV_GetCount()

		GuiControl,, TextSel2, %ListTwo%

	Return
}

ListPurge(ByPass = False){
	Global

	; Remove all assets from List 1 and 2 to start fresh.

	Gui, ListView, ListSel1
	ListOne := LV_GetCount()

	Gui, ListView, ListSel2
	ListTwo := LV_GetCount()
	
	If (ListOne != 0 OR ListTwo != 0) {

		If(!ByPass)
			MsgBox,4,, % LNG.INFO_PURGE_CONF

		If ( MsgBox = Yes OR ByPass = True ) {

			LV_Delete()
			
			Gui, ListView, ListSel1
			LV_Delete()
			
			; Reset Part Numbers
			GuiControl,, PNEnterSel1,
			GuiControl,, PNEnterSel2,
			
			Cur_LoadedLog := ""
		
		} Else {
		
			Return False
		
		}
	
	}

	UpdateCounters()

	Return True
}

SaveLog(cMsg = False){
	Global

	; Loops through the list views that are populated and creates a
	; text document in ScriptDir/Logs in the following format
	;
	; __PartNumber
	; DPT Code:Serial Number:Process Indicator
	; -TimeStamp

	FormatTime, DateString,, MMM dd 
	FormatTime, TimeString,, hmmt

	GuiControlGet, PNEnterSel1

	If (PNEnterSel1 != "") {
		StringReplace, fnPart, PNEnterSel1, -,, All
		fnPartNo := " "PARTS[fnPart]
	} 

	Gui, ListView, ListSel1

	If (LV_GetCount() = 0) {
		Return False
	}

	If (cMsg) {

		NoteMsg := "Append a short note to the front of your log file.`n"
		NoteMsg .= "Do Not Use The Following:   \ / ~ ! @ # $ % ^ & * `n`n"
		NoteMsg .= "Current Log Name:`n" DateString fnPartNo " " TimeString ".txt" 
		
		InputBox, cNote, OB+ Custom Save Note, % NoteMsg
		If InputBox, Ok
		{
			cNote := "_" cNote "_"
		}
		;cNote := RegExReplace( cNote, ["\/:*?<>|"] )
	}

	file := FileOpen(A_ScriptDir "\logs\" DateString cNote fnPartNo "_" TimeString ".txt", "w")

	file.Write("__" PNEnterSel1 "`r`n")

	Loop % LV_GetCount()
	{
		
		LV_GetText(GetDPT, A_Index, 1)
		LV_GetText(GetSN, A_Index, 2)
		LV_GetText(GetPR, A_Index, 3)

		file.Write(GetDPT ":" GetSN ":" GetPR "`r`n")
	
	}

	FormatTime, HumanTime, TimeString,

	file.Write("-Saved On: " HumanTime)

	Cur_LoadedLog := A_ScriptDir "\logs\" DateString cNote fnPartNo "_" TimeString ".txt"

	file.Close()

	Return True
}

LoadLog(SRC){
	Global

	; Loads a log file and populates the GUI with the given values.

	ListPurge()
	
	Sleep, 300 		; The purging seems to fire off way too quick, this is a test.

	Cur_LoadedLog := SRC

	SplitPath, Cur_LoadedLog, LoadedLogName

	Loop, Read, %Cur_LoadedLog%
	{
		If (SubStr(A_LoopReadLine, 1, 2) = "__") {

			StringTrimLeft, LoggedSN, A_LoopReadLine, 2

			If (LoggedSN != "") {	
				GuiControl,, PNEnterSel1, %LoggedSN%
			}
		}

		IfInString, A_LoopReadLine, :
		{

			If (SubStr(A_LoopReadLine, 1, 1) = "-") {		; This is most likely the end-line timestamp.
			
				StringTrimLeft, CulledTs, A_LoopReadLine, 11
				Cur_LoadedTs := CulledTs
				
				Continue
			}

			Gui, ListView, ListSel1

			; Split the line using : as a delimiter and add the values using the provided array.
			StringSplit, DPTSNArr, A_LoopReadLine, :
			LV_Add("", DPTSNArr1, DPTSNArr2, DPTSNArr3)

		}

	}

	ProcessDPT_CurrentList := 1

	Return True
}

ProcSuccess(msg) {

		; Attempt to get the user's attention by targetting their ADHD
		; This will throw a sysBeep, Flash the taskbar icon, and display a message
		; Mind freak, are you ready?

		Gui Flash 						; Flash the taskbar icon
		SoundBeep, 2000					; Make some noise
		SetTimer, ErrorWatcher, -1000   ; Turn off the ErrorWatcher
		BriefMsg(msg, 750)				; Show a nice little message.

		Return
}