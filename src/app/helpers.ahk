ActivateOrion(subwin = "", altk =""){
	Global

	; Activate Orion
	; SubWin = Target Sub-Window
	; AltK   = Alt-Key Sequence to open required window.

	;MsgBox % Opt_LegacyMode

	TargetWindow := !Opt_LegacyMode ? "INVENTORY" : "ahk_exe sbclient.exe"

	IfWinExist, %TargetWindow%
	{
			
		WinActivate
		;WinWaitActive,,,1
			
		If(subwin != "")
		{

			IfWinExist, %subwin%
			{
				WinActivate, %subwin%
				WinWaitActive, %subwin%,, 1

			} 

		}
		
		Return
	
	} else {
		; Throw a fit
	}

}

BriefMsg(msg, slp = 1000){

	; Displays a brief non-obtrusive message to the user.
	; Msg 	= Message to display.
	; Slp 	= Amount of time to keep window visible.

	Gui, BriefMsg:New 

	Gui BriefMsg: +AlwaysOnTop +Disabled -SysMenu +Owner
	Gui BriefMsg:Add, Text,, % msg
	Gui BriefMsg:Show, NoActivate, OB+ Message

	Sleep, %slp%

	Gui, BriefMsg:Destroy

	Return
}

ReadIni(key,section,default){

	; Read the applications local configuration file.

	Global IniName

	ini := IniName
	IniRead, Out, %ini%, %section%, %key%, %default%

	Return Out
}

UpdateIni(key, section, value, default){

	; Update the application's local configuration file.

	Global IniName

	tmp := IniName

	if (value != default){
	
		; Only write the value if it differs from what is already written
	
		if (ReadIni(key,section,-1) != value){
			IniWrite,  %value%, %tmp%, %section%, %key%
		}
	
	} else {
	
		; Only delete the value if there is already a value to delete

		if (ReadIni(key,section,-1) != -1){
			IniDelete, %tmp%, %section%, %key%
		}
	
	}
}

BuildIniName(){

	; The Configs name. Pretty sure this is useless.

	Global IniName

	tmp := A_Scriptname
	
	Stringsplit, tmp, tmp,.
	IniName := ""
	last := ""
	
	Loop, % tmp0 {
	
		if (last != ""){
	
			if (IniName != ""){
				IniName := IniName "."
			}
	
			IniName := IniName last
	
		}
	
		last := tmp%A_Index%
	
	}
	
	IniName .= ".ini"

	Return
}

ClipboardThis(){

	Clipboard = 
	SendInput, ^c
	
	ClipWait, 0.5

	CapturedVal := Clipboard

	Clipboard = 

	Return CapturedVal

}