ProcessDPTListen:

	Go := ProcessDPTStart()
	If(!Go)
	{

		Return

	}

	Sleep, 500

	GoTo, ProcessDPT_Step1

Return

ProcessDPT_Step1:

	Go := ProcessDPTStepOne()
	If(!Go)
	{

		Return

	} else {

		ProcSuccess(LNG.STATUS_COMPLETE)

	}

Return

ProcessDPTStart(){
	Global

	if(ProcessDPTCurrentLoop = "") 
	{
		ProcessDPTCurrentLoop := 1
		ProcessDPT_CurrentList := 1
	}

	Gui, ListView, ListSel%ProcessDPT_CurrentList%

	if(LV_GetCount() = 0)
	{

		SoundPlay *16
		BriefMsg("DPT" LNG.ERR_ABORTING "No Assets Found", 850)

		Return false

	}

	ProcessDPT_LoopCount = % LV_GetCount()

	ActivateOrion("Defective Part Tag Entry") 

	Return True
}

ProcessDPTStepOne(){
	Global

	; I, for one, personally believe there has to be a way to set the thread timing for a 
	; specific function definitively instead of 50+ Sleep calls...

	MissingCodes := Object()

	Gui, ListView, ListSel%ProcessDPT_CurrentList%

	Cur_ProcessMode := "DPT"

	SetTimer, ErrorWatcher, 150

	Loop % ProcessDPT_LoopCount
	{
		Clipboard = 

		; If there's a Serial Number present, move to next entry
		LV_GetText(Proc, A_Index, 2)
		If (Proc != "") {
			Continue
		}

		LV_GetText(DPTEntry, A_Index, 1)
			Sleep, %Opt_WaitTime%

		If(Opt_LegacyMode)
		{

			WinGetText, NewCheck, a
		
		} else {

			Clipboard =
			Send, ^c
			NewCheck := Clipboard

		}	

		If(NewCheck != "NEW`r`n")
		{
			SendInput, ^a
		}
		
		SendInput, %DPTEntry%
			Sleep, %Opt_WaitTime%

		SendInput, {Enter}
			Sleep, %Opt_WaitTime%

		Clipboard =
		SendInput, ^c
		ClipWait, 0.5
		SN := Clipboard
			Sleep, %Opt_WaitTime%

		SendInput, {F5}
			Sleep, %Opt_WaitTime%

		SendInput, {F5}
			Sleep, %Opt_WaitTime%

		WinWaitActive, Repair DPT Update,, 1

		If(Opt_LegacyMode = 1)
		{

			s := ""
			WinGetText, s, a

			; New instructions to completely avoid assets missing 450 codes. 
			Check450 := False

			While(s != "")
			{
				if(s = "450`r`n")
					Check450 := True ; This will indicate that we have found a 450 code.

				Send, {Down}
				Sleep, 500
				WinGetText, s, a 
			}

		} else {

			Check450 := False

			Clipboard = 

			SendInput, ^c
			ClipWait, 1
			s := Clipboard

			While(s != "")
			{
				if(s == "450`r`n" OR s == "450")
					Check450 := True

					Send, {Down}
					Sleep, 500
					Clipboard = 
					SendInput, ^c
					ClipWait, 1
					s := Clipboard
			}
		
		}

		; The 450 Code is missing. 
		; Remove asset from OB+ list and store it in a error array for end of processing report.
		if(Check450 = False)
		{

			SendInput, 450
				Sleep, %Opt_WaitTime%

			SendInput, {Down}
				Sleep, %Opt_WaitTime%

			MissingCodes.Insert(A_Index, DPTEntry) ; need to insert the affected rownumber here
				Sleep, %Opt_WaitTime%

		}

		if(Cur_DPTMode == 200)				; Resale Code Entry
		{
			SendInput, 200
				Sleep, %Opt_WaitTime%

			SendInput, {F2}
				Sleep, %Opt_WaitTime%

			WinWaitActive, SB+ Dialog,, 1

			SendInput, {Enter}
				Sleep, %Opt_WaitTime%

			SendInput, {Enter}
				Sleep, %Opt_WaitTime%

			WinWaitActive, SB+ Prompt,, 1

			SendInput, %Opt_LogCode%
				Sleep, %Opt_WaitTime%
			
			If ( ! IMPULSE101 )
				SendInput, {Enter}

		} 
		else if(Cur_DPTMode == 210)			; Restock Code Entry
		{
			SendInput, 210
				Sleep, %Opt_WaitTime%

			SendInput, {F2}
				Sleep, %Opt_WaitTime%

			SendInput, {Left}
				Sleep, %Opt_WaitTime%

			SendInput, {Enter}
				Sleep, %Opt_WaitTime%

			If ( ! IMPULSE101 )
				SendInput, {Enter}
		}

		/*
		If(!Opt_LegacyMode)	; Le`sigh
		{
			ErrorCount := 0
		}
		*/
	
		; Insert the asset's SN into the list to verify we've processed this.
		LV_Modify(A_Index, "Vis", DPTEntry, SN)

		Sleep, %Opt_WaitTime%
	}

	; Display any assets missing codes to the user so they can make record of them
	If(MissingCodes.Length() > 0)
	{
		MissingError := "Warning.`n`nThe Following DPT Tags were missing 450 codes and have been manually entered under your alias.`n`n"
		
		For Index, Value in MissingCodes
		{
			MissingError .= Value "`n"
		}

		MissingError .= "`nIt is suggested that you personally record these assets in case of any problems that may arise."
		
		MsgBox,, OB+ Removed Assets, % MissingError
	}	

	Return True
}

ProcessPalListen:

	Go := ProcessPalStart()
	If(!Go)
	{

		Return

	} else {

		ProcSuccess("Pallet" LNG.STATUS_COMPLETE)

	}

Return

ProcessPalStart(){
	Global

	Cur_ProcessMode := "Pal"

	if(ProcessDPT_CurrentList = "") 
		ProcessDPT_CurrentList := 1
	
	SetTimer, ErrorWatcher, 150

	GuiControlGet, PNEnterSel%list%
	PartNum := PNEnterSel%list%

	Gui, ListView, ListSel%ProcessDPT_CurrentList%

	if(LV_GetCount() = 0)
	{

		SoundPlay *16
		BriefMsg("Pallet" LNG.ERR_ABORTING "No Assets Found", 850)
		
		Return false
	}

	ProcessDPT_LoopCount = % LV_GetCount()

	;MsgBox,, OB+Dialog: Verify Pallet Processing, Ensure "Build Pallet" is open. 
	;if ErrorLevel
	;	Return False

	ActivateOrion("Build Pallet") 
	Sleep, 1000

	Loop % ProcessDPT_LoopCount
	{

		LV_GetText(Proc, A_Index, 3)
		If(Proc != "") ; Generally this should always be an X
			Continue

		LV_GetText(SerialN, A_Index, 2)

		SendInput, %SerialN%
		Sleep, %Opt_PalWait%
		SendInput, {Enter}

		LV_Modify(A_Index, "Col3", "X")

	}

	Return True

}