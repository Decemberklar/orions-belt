ErrorWatcher(){
	Global

	ErrorCountLimit := 10

	; ErrorWatcher is used during DPT/Pallet processing.
	; This will watch for pop-ups, specifically SB+ Error and Interrupt OB+ so the 
	; end-user can verify/fix any possible issues.
	; SetTimer, ErrorWatcher, [Interval]

	; Personal debate as to if I want to keep this watcher around.
	;If WinExist("SB+ Dialog")
	;{
	;	ErrorCount := ErrorCount + 1
	;}

	/*
	If(!Opt_LegacyMode)
	{

		ErrorCountLimit := 1000

		ErrorSleep := Opt_WaitTime - 100

		IfWinExist, INVENTORY
		{
		
			IfWinNotActive
			{
		
				WinGetActiveTitle, CurrentActiveWindow

				If(CurrentActiveWindow == "")
				{

					Sleep, %ErrorSleep%
					If(CurrentActiveWindow == "")
					{
						ErrorCount := ErrorCount + 1
					}
				}
		
			}
		
		}

	}
	*/

	; SB+ Error is in fact an error, so immediately interrupt.
	If WinExist("SB+ Error")
	{
		ErrorCount := 2 * ErrorCountLimit
	}

	; SB+ Prompt is the end of a current thread (code entry). Reset the number.
	If WinExist("SB+ Prompt")
	{
		ErrorCount := 0
	}

	; It's going down.
	If(ErrorCount >= ErrorCountLimit)
	{
		MsgBox, 0x41000, OB+: Error Detected, % LNG.ERR_COMMUNICATION_ISSUES
		IfMsgBox Ok
		{
		
			ErrorCount := 0

			Process%Cur_ProcessMode%StepOne()
		
		}
	}
}