ProcessDPTListen:
	Go := ProcessDPTStart()
	If (!Go) {
		Return
	}
	Sleep, 500
	GoTo, ProcessDPT_Step1
Return

ProcessDPT_Step1:
	Go := ProcessDPTStepOne()
	If (!Go) {
		Return
	}
	ProcSuccess(LNG.STATUS_COMPLETE)
Return

ProcessDPTStart(){
	Global

	Gui, ListView, ListSel1
	L1 := LV_GetCount()
	
	Gui, ListView, ListSel2
	L2 := LV_GetCount()

	If (L1 == 0 AND L2 == 0)
	{
		SoundPlay *16
		BriefMsg("DPT" LNG.ERR_ABORTING "No Assets Found", 850)

		Return False
	}

	ActivateOrion("Defective Part Tag Entry")

	Return True

}

ProcessDPTStepOne(){
	Global

	MissingCodes	   := Object()
	Cur_ProcessingMode := "DPT"

	CL  := 1
	INT := 1

	SetTimer, ErrorWatcher, 150

	While(CL = 1 
	 OR CL = 2)
	{

		; Finishing Block
		If (INT >= L%CL%) {

			If (CL = 2) {

				ToolTip,		; Discard The Tooltip

				; Cycle through MissingCodes and display them to user.
				If (MissingCodes.Length() > 0) {

					MissingError := "Warning`n`n"
					MissingError .= "The following assets were missing 450 codes and were manually entered under your alias."

					For Index, Value in MissingCodes 
					{ 
						MissingError .= Value "`n"
					}
					
					MissingError .= "`nIt is recommended that you make record of the tag(s) in-case of future errors."
					
					MsgBox,, OB+ Missing Codes, % MissingError
				
				}

				Return True
			}

			CL 	:= 2
			INT := 1

			; Continue   ; Let's remove this and see.

		}

		Gui, ListView, ListSel%CL%

		Loop % LV_GetCount()
		{

			ToolTip % INT "/" LV_GetCount()

			Clipboard =

			LV_GetText(SN_Check, A_Index, 2)
			If (SN_Check != "") {
				INT++
				Continue
			}

			LV_GetText(DPTEntry, A_Index, 1)
				Sleep, %Opt_WaitTime%

			If (Opt_LegacyMode) {
				WinGetText, NEW_Check, A
			} else {
				NEW_Check := ClipboardThis()
			}

			If (NEW_Check != "NEW`r`n") {
				SendInput, ^a
				Sleep, 300
			}

			SendInput, %DPTEntry%
				Sleep, %Opt_WaitTime%

			SendInput, {Enter}
				Sleep, %Opt_WaitTime%

			SerialNumber := ClipboardThis()
				Sleep, %Opt_WaitTime%

			SendInput, {F5}
				Sleep, %Opt_WaitTime%

			SendInput, {F5}
				Sleep, %Opt_WaitTime%

			WinWaitActive, Repair DPT Update,, 1

			If (Opt_LegacyMode = 1) {

				450_Check := ""

				WinGetText, 450_Check, A

				Check450 := False

				While (450_Check != "") {
					
					If(450_Check == "450`r`n")
						Check450 := True


					SendInput, {Down}
					Sleep, 500
					WinGetText, 450_Check, A
					Sleep, 300

				}

			} else {

				Check450 := False

				450_Check := ClipboardThis()

				While (450_Check != "") {

					If (450_Check == "450`r`n" OR 450_Check == "450")
						Check450 := True

					SendInput, {Down}
					Sleep, 500
					450_Check := ClipboardThis()
					Sleep, 300
				}

			}

			If (Check450 == False) {

				SendInput, 450
					Sleep, %Opt_WaitTime%

				SendInput, {Down}
					Sleep, %Opt_WaitTime%

				MissingCodes.Insert(A_Index, DPTEntry)
					Sleep, %Opt_WaitTime%

			}

			If (Cur_DPTMode == 200) {

				SendInput, 200
					Sleep, %Opt_WaitTime%

				SendInput, {F2}
					Sleep, %Opt_WaitTime%

				If (Opt_LegacyMode = 1){
					WinWaitActive, SB+ Dialog,, 1
				}

				SendInput, {Enter}
					Sleep, %Opt_WaitTime%

				SendInput, {Enter}
					Sleep, %Opt_WaitTime%

				If (Opt_LegacyMode = 1){
					WinWaitActive, SB+ Prompt,, 1
				}

				SendInput, %Opt_LogCode%
					Sleep, %Opt_WaitTime%

				If (!IMPULSE101)
					SendInput, {Enter}
						Sleep, %Opt_WaitTime%

			} Else If (Cur_DPTMode = 210) {

				SendInput, 210
					Sleep, %Opt_WaitTime%

				SendInput, {F2}
					Sleep, %Opt_WaitTime%

				SendInput, {Left}
					Sleep, %Opt_WaitTime%

				SendInput, {Enter}
					Sleep, %Opt_WaitTime%
				
				If (!IMPULSE101)
					SendInput, {Enter}
						Sleep, %Opt_WaitTime%

			}

			If ( ! Opt_LegacyMode ) {
				ErrorCount := 0
			}

			LV_Modify(A_Index, "Vis", DPTEntry, SerialNumber)
				Sleep, %Opt_WaitTime%

			INT++

		}

	}

}

ProcessPalListen:

	Gui, ListView, ListSel1
		L1 := LV_GetCount()
	Gui, ListView, ListSel2
		L2 := LV_GetCount()

	If (L1 == 0 AND L2 == 0) {
		SoundPlay *16
		BriefMsg("Pallet" LNG.ERR_ABORTING "No Assets Found", 850)

		Return
	}

	Go := ProcessPalStepOne()
	If (!Go)
	{
		Return

	} else {

		ProcSuccess("Pallet" LNG.STATUS_COMPLETE)

	}

Return

ProcessPalStepOne(){
	Global

	Cur_ProcessingMode := "Pal"

	CL  := 1
	INT := 1

	SetTimer, ErrorWatcher, 150

	While(CL = 1 
	 OR CL = 2)
	{

		; Finishing Block
		If (INT >= L%CL%) {

			If (CL = 2) {

				Return True

			}

			CL 	:= 2
			INT := 1

		}

		Gui, ListView, ListSel%CL%

		ActivateOrion("Build Pallet")
		Sleep, 1000

		Loop % LV_GetCount()
		{

			LV_GetText(Proc, A_Index, 3)
			If (Proc != "") {
				INT++
				Continue
			}

			LV_GetText(SerialN, A_Index, 2)
			If (SerialN = "") {
				INT++
				Continue
			} 

			SendInput, %SerialN%
			Sleep, %Opt_PalWait%
			SendInput, {Enter}


			LV_Modify(A_Index, "Col3", "X")

			INT++

		}

	}

}