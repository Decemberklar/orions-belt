; This array is used to connect a human readable name to log files.
PARTS := {  LP1815135: "Lenovo.T420"
			, LP1815294: "Lenovo.T430"
			, LP1815447: "Lenovo.T440"
			, LP1815136: "Lenovo.X220"
			, LP1815300: "Lenovo.X230"
			, CP0514776: "HP.6005"
			, CP0315338: "HP.6305"
			, LP3014953: "HP.8440"
			, LP3015500: "HP.840"
			, LP3015538: "HP.840.T"
			, CP0515574: "HP.Mini"
			, LP2615319: "ToughBook.MK6"
			, LP2615521: "Toughbook.MK7"
			, CP1915597: "Dell.Mini"
			, LP0315519: "MSurfacePro" }
			
; System Strings
LNG := { ERR_COMMUNICATION_ISSUES: "OB+ is having issues communicating with Orion.`n`nPlease check Orion for any possible stuck dialogs/errors.`n`nPress OK to proceed, or Cancel to cancel processing."
		, ERR_ABORTING: " Processing Aborted. Reason: "
		, ERR_INVALID: "Entry Rejected. Reason: Invalid"
		, STATUS_SAVE_PURGE : "Log Successfully Saved.`n`nPurge Current List Contents?"
	   	, STATUS_DPT_CHANGED: "DPT Mode Successfully Changed: " 
	   	, STATUS_COMPLETE: " Processing Successfully Completed." 
	   	, INFO_PURGE_CONF: "Data that is not saved will be lost.`n`nContinue?" }