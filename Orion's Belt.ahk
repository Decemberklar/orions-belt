#SingleInstance, FORCE 
#NoEnv

BuildIniName()

appName			:= "Orion's Belt"
appVersion		:= "Betelgeuse"
appRelease		:= "1.3.0"

Cur_DPTMode		:= 200
Cur_SelList		:= ""	; @todo notused? has uses but never seems "used"
Cur_LoadedLog   := ""
Cur_LoadedTs	:= ""
Cur_ProcessMode := ""

Opt_LogCode     := ReadIni("200LogCode" , "Options" , "LOG04")
Opt_WaitTime    := ReadIni("DPTWaitTimes", "Options", "1000")
Opt_ScanWait    := ReadIni("ScannerListen", "Options", "100")
Opt_ScanWaitInc := Opt_ScanWait + 50
Opt_PalWait		:= ReadIni("PALWaitTimes", "Options", "500")
Opt_LegacyMode  := ReadIni("LegacyMode", "Options", 1)

ProcessDPTCurrentLoop  := ""	; Not terribly sure any of these 3 are required...
ProcessDPT_CurrentList := ""	
ProcessDPT_LoopCount   := ""	

ErrorCount 		:= 0

BuildGUI()

#Include, %A_ScriptDir%/src/app/lang.ahk
#Include, %A_ScriptDir%/src/app/helpers.ahk
#Include, %A_ScriptDir%/src/ui/gui.ahk
;#Include, %A_ScriptDir%/src/app/processing.ahk
#Include, %A_ScriptDir%/src/app/NEWPROCESSING.ahk
#Include, %A_ScriptDir%/src/app/errorhandling.ahk
#Include, %A_ScriptDir%/debug.ahk

IniName 		:= ""
BuildIniName()

Return

; TESTING SECTION
^!NumpadEnd::
Return
